require('./global.js');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');

const app = express();

app.use(
    bodyParser.urlencoded({
        extended: false,
        parameterLimit: 10000, //giới hạn truyền vào (kb)
        limit: 1024 * 1024 * 10
    })
);
app.use(
    bodyParser.json({
        extended: false,
        parameterLimit: 10000,
        limit: 1024 * 1024 * 10
    })
);
app.use(fileUpload({
    createParentPath: true //nếu chưa có folder thì tự tạo
}));

app.use(require("./controllers"));

const configs = GET_APPLICATION();

app.listen(configs.port, configs.ip, function(e) {
    console.log("Started server");
    mongoose.connect('mongodb://localhost:27017/mongoSearch', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
});
