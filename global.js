global.GET_APPLICATION = function () {
    var app = {};
    switch (process.env.ENVIROMENT) {
        case "dev":
            app.ip = "0.0.0.0";
            app.port = 8080;
            break;
        case "build":
            app.ip = "0.0.0.0";
            app.port = 8080;
            break;
        default:
            app.ip = "0.0.0.0";
            app.port = 8080;
            break;
    }
    return app;
}
