const mongoose = require('mongoose');

const Product2Schema = new mongoose.Schema({
  product_category_id: {
    type: String,
    required: false
  },
  name: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    required: true
  },
  desc: {
    type: String,
    required: false
  },
  detail: {
    type: String,
    required: false
  },
  avatar: {
    type: String,
  },
  image_arr: {
    type: [],
  },
  price: {
    type: Number,
  },
  /* Thương hiệu */
  trademark: {
    type: Number,
  },
  /* Chất liệu */
  material: {
    type: Number,
  },
  /* Xuất xứ */
  Origin: {
    type: Number,
  },
  status: {
    type: Number,
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  }
});

Product2Schema.index({
  slug: "text",
  type: -1
})

Product2Schema.index({
  name: "text",
  type: -1
})

module.exports = mongoose.model('Product', Product2Schema);
