const mongoose = require("mongoose");
const fs = require("fs");

(async () => {
    const db = await mongoose.connect("mongodb://127.0.0.1/trees?authSource=admin", {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    const Menu = new mongoose.Schema(
        {
            name: {
                type: String,
                required: true,
            },
            url: {
                type: String,
                required: true,
            },
            parentId: {
                type: mongoose.Schema.Types.ObjectId,
            },
        },
        {
            versionKey: false,
            collection: "menus",
        }
    );
    db.model("Menu", Menu);

    const createTestData = async function () {
        let counter = await db.models.Menu.countDocuments({});
        if (counter == 0) {
            let homepage = await db.models.Menu.create({ name: "Homepage", url: "/" });

            // DEPTH 0
            let homepage1 = await db.models.Menu.create({ name: "Homepage 1", url: "/1", parentId: homepage._id });
            let homepage2 = await db.models.Menu.create({ name: "Homepage 2", url: "/2", parentId: homepage._id });

            // DEPTH 1
            let homepage11 = await db.models.Menu.create({ name: "Homepage 11", url: "/1/1", parentId: homepage1._id });
            let homepage12 = await db.models.Menu.create({ name: "Homepage 12", url: "/1/2", parentId: homepage1._id });

            // DEPTH 2
            let homepage111 = await db.models.Menu.create({ name: "Homepage 111", url: "/1/1/1", parentId: homepage11._id });
        }
    };
    await createTestData();

    let menus = await db.models.Menu.aggregate([
        {
            $match: {
                parentId: null, // Thằng này sẽ lấy ra tất cả menu gốc
            },
        },
        {
            $graphLookup: {
                // Thằng này dùng đệ quy để lấy ra hết tổ tông con cháu của nó
                from: "menus",
                startWith: "$_id",
                connectFromField: "_id",
                connectToField: "parentId",
                depthField: "depth",
                as: "submenus",
            },
        },
    ]);
    const getSubmenu = function (menu, totalSubmenus) {
        return totalSubmenus
            .filter((item) => {
                return item.parentId.toString() == menu._id.toString();
            })
            .map((item) => {
                item = Object.assign({}, item);
                item.children = getSubmenu(item, totalSubmenus);
                return item;
            });
    };
    let results = [];
    for (let i = 0; i < menus.length; i++) {
        let menu = Object.assign({}, menus[i]);
        menu.children = getSubmenu(menu, menu.submenus);
        delete menu.submenus;
        results.push(menu);
    }
    await fs.writeFileSync(`${new Date().getTime()}.json`, JSON.stringify(results, null, 2), "utf-8");
})();
