const express = require('express');
const http = require('http');
const Models = require('../models');
const router = express.Router();
const request = require('request');

router.post('/create', async function(req, res, next) {
  console.log(11111);
  let product = await Models.Product.create(req.body);

  return res.json({
    status: 200,
    message: `success: ${product._id}`
  });
});

router.get('/filter', async function(req, res, next) {
  await Models.Product.find({
    slug: req.query.slug,
    price: req.query.price
  }, async function(err, user) {
    if (err) {
      return res.json({
        status: 404,
        message: err
      });
    }
    return res.json({
      status: 200,
      message: user
    })
  });
});

router.get('/price', async function(req, res, next) {

  let minPrice = req.query.minPrice
  let maxPrice = req.query.maxPrice
  let slug = req.query.slug
  let categoryId = req.query.category_id

  await Models.Product.find({
    "product_category_id": categoryId,
    "price": {
      $gt: minPrice,
      $lt: maxPrice
    },
    "slug": {
      $regex: slug
    },
  }, async function(err, product) {
    if (err) {
      return res.json({
        status: 404,
        message: err
      });
    }
    return res.json({
      status: 200,
      message: product
    })
  });
});

router.get('/fulltextsearch', async function(req, res, next) {
  let slug = req.query.slug
  let name = req.query.name
  let detail = req.query.detail

  await Models.Product.find({
    $text: {
      $search: detail
    }
  }, async function(err, product) {
    if (err) {
      return res.json({
        status: 404,
        message: err
      });
    }
    return res.json({
      status: 200,
      size: product.length,
      message: product
    })
  });
});

router.post('/testpost', async function(req, res, next) {
  console.log(req.body);
  const requestAPI = function(options){
    return new Promise((resolve) => {
      try {
        request(options, function(error, response){
          let text = response && response.body ? response.body : null;
          let body = text ? JSON.parse(text) : {};
          resolve(body);
        })
      } catch (e){
        console.log(e);
        resolve({});
      }
    });
  }
  let options = {
    method: "POST",
    url: "http://localhost:8080/api/v1/product/create",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(req.body)
  }
  let response = await requestAPI(options);
  return res.json(response);


  // // Mongo Local
  // var product = await Models.Product.create(req.body);
  //
  // // Mongo Object
  // console.log(product);
  //
  // // JS Object
  // console.log(product.toObject());
  //
  // // Tìm và trả về mongo object
  // var products = await Models.Product.find({});
  //
  // // Tìm và trả về JS object
  // var products2 = await Models.Product.find({}).lean();
  //
  // console.log(products);
  // console.log(products2);

});

router.get('/test', function(req, res){
  var request = http.request({
    host: 'localhost',
    port: 8080,
    path: '/api/v1/product/getall',
    method: 'GET'
  }, function(response) {
    var data = '';
    response.setEncoding('utf8');
    response.on('data', (chunk) => {
      data += chunk;
    });
    response.on('end', () => {
      res.end(data);
    });
  });
  request.end();
});

router.get('/getall', async function(req, res, next) {
  await Models.Product.find({}, async function(err, product) {
    console.log(err);
    if (err) {
      return res.json({
        status: 404,
        message: err
      });
    }
    return res.json({
      status: 200,
      size: product.length,
      message: product
    })
  });
});

module.exports = router;
