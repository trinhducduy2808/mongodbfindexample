const express = require('express');
const router = express.Router();
const path = "/api/v1";

router.use(`${path}/product`, require("./upload.js"));

module.exports = router;
