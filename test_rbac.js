const express = require('express');
const node_acl = require('acl');

let app = express();
app.set('port', process.env.PORT || 3000);

app.use((err, req, res, next) => {
  if (!err) return next();
  req.status(403).json({
    message: err.msg,
    error: err.errorCode
  });
});

let acl = new node_acl(new node_acl.memoryBackend(), {
  debug: (msg) => {
    console.log('-DEBUG-', msg);
  }
});

// This creates a set of roles which have permissions on
//  different resources.
acl.allow([{
    roles: 'trumcuoi',
    allows: [{
        resources: '/allow',
        permissions: '*'
      },
      {
        resources: '/disallow',
        permissions: '*'
      },
    ]
  },
  {
    roles: 'manager',
    allows: [{
      resources: '/posts/publish',
      permissions: '*'
    }]
  },
  {
    roles: 'writer',
    allows: [{
      resources: '/posts',
      permissions: 'post'
    }]
  }, {
    roles: 'guest',
    allows: [{
      resources: '/posts',
      permissions: 'get'
    }]
  }
]);

// Inherit roles
//  Every writer is allowed to do what guests do
//  Every manager is allowed to do what writer do
acl.addRoleParents('writer', 'guest');
acl.addRoleParents('manager', 'writer');

initRoutes();

function initRoutes() {
  // Defining routes ( resources )

  // Simple overview of granted permissions
  app.get('/info', (req, res) => {
    acl.allowedPermissions(getUserId(req), ['/posts', '/posts/publish', '/allow/*', '/disallow/*'], (err, permission) => {
      res.json(permission);
    });
  });

  app.get('/user', (req, res) => {
    acl.userRoles(getUserId(req), (err, roles) => {
      res.json(roles);
    })
  });

  // Only for guests and higher
  app.get('/posts', acl.middleware(1, getUserId), (req, res) => {
    res.send('Read post!');
  });

  // Only for writer and higher
  app.post('/posts', acl.middleware(1, getUserId), (req, res) => {
    res.send('Post created!');
  });

  // Only for manager
  app.post('/posts/publish', acl.middleware(0, getUserId), (req, res) => {
    res.send('Post published!');
  });

  // Setting a new role
  app.get('/allow/:user/:role', acl.middleware(1, getUserId), (req, res) => {
    acl.addUserRoles(req.params.user, req.params.role);
    res.send(req.params.user + ' is a ' + req.params.role);
  });

  // Unsetting a role
  app.get('/disallow/:user/:role', acl.middleware(1, getUserId), (req, res) => {
    acl.removeUserRoles(req.params.user, req.params.role);
    res.send(req.params.user + ' is not a ' + req.params.role + ' anymore.');
  });
}

// grant "trumcuoi" role for default user
acl.addUserRoles("boss", "trumcuoi");

// grant "manager" role
acl.addUserRoles("duytd", "manager");

// grant "writer" role
acl.addUserRoles("thiepdv", "writer");

// grant "guest" role
acl.addUserRoles("testGuest", "guest");

// Provide logic for getting the logged-in user
//  This is a job for your authentication layer
function getUserId(req) {
  return req.query.uid; // (yaoming) just for fun
}

app.listen(app.get('port'), () => {
  console.log('ACL example listening on port ' + app.get('port'));
});

// https://viblo.asia/p/expressjs-phan-quyen-theo-vai-tro-voi-package-node-acl-6J3Zg29qKmB
